/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceanimal;

/**
 *
 * @author BOAT
 */
public class Cat extends LandAnimal {

    private String nickname;

    public Cat(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
       System.out.println("Cat: "+nickname+" eat");
    }

    @Override
    public void speak() {
        System.out.println("Cat: "+nickname+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: "+nickname+" sleep");
    }

    @Override
    public void run() {
         System.out.println("Cat: "+nickname+" run");
    }

}
