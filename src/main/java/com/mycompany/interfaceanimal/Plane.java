/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceanimal;

/**
 *
 * @author BOAT
 */
public class Plane extends Vahicle implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
        
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: stopEngine");
    }

    @Override
    public void ralseSpeed() {
        System.out.println("Plane: ralseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: applyBreak");
    }

    @Override
    public void fly() {
        System.out.println("Plane: fly");
    }

    @Override
    public void run() {
       System.out.println("Plane: run");
 
    }
    
}
