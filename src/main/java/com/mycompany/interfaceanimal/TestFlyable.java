/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceanimal;

/**
 *
 * @author BOAT
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("Boat");
        Bird bird = new Bird("Garo");
        Plane plane = new Plane("Engine number 1");
        Car car = new Car("Engine number 2");
        //bat.fly();//Animal,Poultry,Flyable
        //plane.fly();//Vahicle,Flyable
        Dog dog = new Dog("solo");
        Cat cat = new Cat("nami");

        Flyable[] flyables =
        {
            bat, plane, bird,
        };
        for (Flyable f : flyables)
        {
            if (f instanceof Plane)
            {
                Plane p = (Plane) f;
                p.startEngine();
                f.fly();
                p.stopEngine();
                p.ralseSpeed();
                p.applyBreak();

            }
            f.fly();
        }

        Runable[] runables =
        {
            dog, cat, car
        };
        for (Runable r : runables)
        {
            if (r instanceof Car)
            {
                Car c = (Car) r;
                c.startEngine();
                c.run();
                c.stopEngine();
                c.ralseSpeed();
                c.applyBreak();
            }
            r.run();
        }

    }
}
