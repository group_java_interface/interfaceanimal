/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceanimal;

/**
 *
 * @author BOAT
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: stopEngine");
    }

    @Override
    public void ralseSpeed() {
        System.out.println("Car: ralseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: applyBreak");
    }

    @Override
    public void run() {
        System.out.println("Car: run");
    }

}
